from setuptools import setup, find_packages


setup(
        name='tdd-example-app', 
        version='1.0', 
        description='A tdd-example-app test code module', 
        author='Brett Alvarez', 
        author_email='brett@foo.com',
	package_dir={'': 'src'},
        packages=find_packages('src')
)
