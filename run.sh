#!/bin/bash

python3.9 -m venv .venv
source .venv/bin/activate
pip install wheel
pip install -r requirements.txt
pip install .
clear
python src/app/main.py
deactivate
rm -rf .venv
rm -rf htmlcov
rm -rf tests/__pycache__
rm -rf src/app/__pycache__
rm -rf .pytest_cache
rm -rf src/tdd*

